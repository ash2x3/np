#![deny(missing_debug_implementations, missing_docs)]
//! np
//!
//! Monitors a window's title and outputs it regularly to a file or stdout.
use regex::{Captures, Regex};
use std::{
    ffi::{OsStr, OsString},
    fmt::{self, Display, Formatter},
    fs,
    io::{self, stdin},
    num,
    path::PathBuf,
    thread,
    time::Duration,
};
use structopt::StructOpt;
use tracing::{error, info};
mod window;
use window::{get_open_windows, get_title, WindowTitle};

/// Errors resulting from parsing the user's menu selection
#[derive(thiserror::Error, Debug)]
enum PromptError {
    #[error("IO error: {0}")]
    Readline(#[from] io::Error),
    #[error("Parse error: {0}")]
    Parse(#[from] num::ParseIntError),
    #[error("Number is out of bounds.")]
    OutOfBounds,
}

/// Prompts the user for an entry in the list, and validates.
fn prompt<T>(input: &mut String, list: &[T]) -> Result<usize, PromptError> {
    input.clear();
    stdin().read_line(input)?;
    // rust is fn magic
    let i: usize = input.trim().parse()?;
    if i >= list.len() {
        return Err(PromptError::OutOfBounds);
    }
    Ok(i)
}

#[derive(Debug)]
enum PathSpec {
    FilePath(PathBuf),
    Stdout,
}
impl Default for PathSpec {
    fn default() -> Self {
        Self::FilePath(PathBuf::from("now_playing.txt"))
    }
}
impl Display for PathSpec {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::FilePath(x) => write!(f, "{}", x.display())?,
            Self::Stdout => write!(f, "-")?,
        }
        Ok(())
    }
}

#[derive(Debug, StructOpt)]
#[structopt(about)]
struct Opt {
    #[structopt(
        short = "o",
        long = "output-file",
        parse(from_os_str = parse_path_spec),
        default_value
    )]
    output_file: PathSpec,

    #[structopt(
        short = "i",
        long = "interval",
        help = "How often (in milliseconds) to update",
        parse(try_from_os_str = parse_interval),
        default_value = "1000"
    )]
    interval: Duration,

    #[structopt(
        long = "regex",
        help = "Regular expression to rename window using capture groups",
        parse(try_from_str = Regex::new)
    )]
    regex: Option<Regex>,
}

/// Parse the specified duration
// TODO: specify using human-written values
//  i.e. "1000ms", "1s", and so on.
// TODO: option for window handle
fn parse_interval(arg: &OsStr) -> Result<Duration, OsString> {
    let as_str = arg.to_str();
    let num: Result<u64, _> = match as_str {
        Some(x) => x.parse(),
        None => return Err(arg.to_owned()),
    };
    num.map(Duration::from_millis)
        .map_err(|pie| OsString::from(format!("{}", pie)))
}

/// Parse the "output path", which can be a file path, or "-" for stdout
fn parse_path_spec(arg: &OsStr) -> PathSpec {
    match arg.to_str() {
        Some("-") => PathSpec::Stdout,
        Some(x) => PathSpec::FilePath(PathBuf::from(x)),
        None => {
            eprintln!("Path is not valid UTF-8.");
            // TODO: prompt user to cancel?
            eprintln!("Parsing lossily.");
            let st = arg.to_string_lossy();
            let buf = PathBuf::from(st.to_string());
            PathSpec::FilePath(buf)
        }
    }
}

// TODO: break this down into more functions and/or modules
fn main() {
    tracing_subscriber::fmt::init();

    let opt = Opt::from_args();

    info!("opt = {:#?}", opt);

    let win_list: Vec<WindowTitle> = get_open_windows();

    if win_list.is_empty() {
        eprintln!("No windows found.");
        panic!("List of open windows was empty");
    }

    // List out the options.
    // In future we could put this inside prompt, so it lists them every time
    eprintln!("Open windows:");
    for (i, WindowTitle(_, name)) in win_list.iter().enumerate() {
        eprintln!(" {i} {name}");
    }
    eprintln!("Please choose a window (0-{})", win_list.len() - 1);

    // Ask the user for a choice until we get a valid one
    let mut input_buffer = String::with_capacity(2);
    let index = loop {
        eprint!("> ");
        let res = prompt(&mut input_buffer, &win_list);
        let index = match res {
            Ok(x) => x,
            Err(e) => {
                eprintln!("{}", e);
                continue;
            }
        };
        break index;
    };

    eprintln!("Configuration:");

    let replacer = |groups: &Captures| {
        let mut st = String::new();
        for group in groups.iter().skip(1).flatten() {
            st.push_str(group.as_str());
        }
        st
    };

    // SAFETY: We've already bounds checked inside prompt()
    let WindowTitle(handle, name) = unsafe { win_list.get_unchecked(index) };
    eprintln!(" Tracking {} ({:?})", name, handle);
    eprintln!(" Updating every {:?}", opt.interval);
    match &opt.output_file {
        PathSpec::Stdout => eprintln!(" Writing to standard output"),
        PathSpec::FilePath(x) => eprintln!(" Writing to {}", x.display()),
    };
    if let Some(rx) = &opt.regex {
        eprintln!(
            " Substituting using regex `{rx}` (example: {})",
            rx.replace(name, replacer)
        );
    }
    // TODO: Regex substitution for titles
    // TODO: interactively edit any of these
    eprintln!("Press Return to start.");
    {
        let mut s = String::new();
        let _res = stdin().read_line(&mut s);
        if let Err(e) = _res {
            error!("{e}");
        }
    }

    let mut buf: [u16; 512] = [0; 512];

    // Loops until the window closes, or the window title is empty.
    // Both are treated the same by [`get_title`].
    while let Some(name) = get_title(*handle, &mut buf) {
        let output = match &opt.regex {
            None => name,
            Some(rx) => rx.replace(&name, replacer).to_string(),
        };
        match &opt.output_file {
            PathSpec::Stdout => {
                println!("{}", output);
            }
            PathSpec::FilePath(path) => {
                if let Err(e) = fs::write(path, output) {
                    let path_clean = path.display();
                    eprintln!("Error writing to {path_clean}: {e}");
                    error!("{e}");
                }
            }
        };
        thread::sleep(opt.interval);
    }
}
