use tracing::{error, info, warn};
use windows::Win32::{
    Foundation::{GetLastError, BOOL, HWND, LPARAM},
    UI::WindowsAndMessaging::{EnumWindows, GetWindowTextW, IsWindowVisible},
};

/// A record to map a window handle ([`HWND`]) to its title.
#[derive(Debug)]
pub struct WindowTitle(pub HWND, pub String);

/// Structure to contain the list of window titles, and a char buffer to give to the system.
/// Used as the data [`LPARAM`] for the [`EnumWindows`] callback.
#[derive(Debug)]
struct CallbackData(Vec<WindowTitle>, [u16; 512]);

/// Finds the titles of all open windows.
pub fn get_open_windows() -> Vec<WindowTitle> {
    let mut data = CallbackData(vec![], [0; 512]);

    // create an `LPARAM(isize)` pointer to data
    let lp = LPARAM(&mut data as *mut _ as _);

    // SAFETY: `lp` is a pointer to a valid initialised value
    unsafe {
        EnumWindows(Some(record_title), lp);
    }
    data.0
}

/// Looks up the title of a given window handle and writes it to the list.
/// Used as the callback to [`EnumWindows`].
extern "system" fn record_title(handle: HWND, data: LPARAM) -> BOOL {
    // SAFETY: we're dereferencing a list that is definitely initialised in caller
    let CallbackData(ref mut list, ref mut buf) = unsafe { &mut *(data.0 as *mut CallbackData) };
    if let Some(title) = get_title(handle, buf) {
        info!("{handle:?}: {title}");
        list.push(WindowTitle(handle, title));
    }
    true.into()
}

/// Does the work of looking up a window's title and responding to errors.
pub fn get_title<const N: usize>(handle: HWND, buf: &mut [u16; N]) -> Option<String> {
    if !unsafe { IsWindowVisible(handle) }.as_bool() {
        return None;
    }

    // SAFETY: cannot overflow the array because of nMaxCount
    info!("GetWindowTextW({handle:?}, ...)");
    let len = unsafe { GetWindowTextW(handle, buf) };
    if len == 0 {
        let rawerr = unsafe { GetLastError() };
        let errno = rawerr.0;
        let err: windows::core::Error = rawerr.into();
        if errno == 0 {
            // Probably means that the window title is empty
            info!("Task failed successfully. {err}");
            None
        } else if (errno >> 29) & 1 == 1 {
            // 29th bit set means app error. since this app does not ever throw these, this should not happen.
            unreachable!("Unexpected app error. {err} [{errno}]")
        } else if errno == 203 {
            // I do not see it...
            warn!("{err} [{errno}]");
            None
        } else {
            error!("{err} [{errno}]");
            None
        }
    } else {
        Some(String::from_utf16_lossy(&buf[..len as usize]))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn no_segfault() {
        let w = get_open_windows();
        println!("{:?}", w);
    }
}
